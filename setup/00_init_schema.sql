-- First create database using the Knoema Economical Data Atlas
-- Go to Marketplace to get database

CREATE VIEW IF NOT EXISTS BEANIPA ("Table", Table_Name, Table_Description, Table_Full_Name, Table_Unit, Indicator, Indicator_Name, Indicator_Description, Indicator_Full_Name, Units, Scale, Frequency, Date, Value) 
as
SELECT * FROM ECONOMY_DATA_ATLAS.ECONOMY.BEANIPA;

create or replace procedure train_model(model_filepath STRING)
    returns string
    language python
    runtime_version = '3.8'
    PACKAGES = ('snowflake-snowpark-python', 'scikit-learn', 'pandas', 'cachetools')
    handler = 'snowpark_devops.procedure.process.train_model'
    imports = ('@deploy/app.zip');
   
create or replace procedure register_model_to_udf(model_filepath STRING
                                                   , udf_name STRING)
    returns string
    language python
    runtime_version = '3.8'
    PACKAGES = ('snowflake-snowpark-python', 'scikit-learn', 'pandas', 'cachetools')
    handler = 'snowpark_devops.procedure.process.register_udf'
    imports = ('@deploy/app.zip');
