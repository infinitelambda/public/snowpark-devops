#!/usr/bin/env python
# coding: utf-8
import os
import sys

import cachetools
import pandas as pd
import snowflake.snowpark.functions as F
import snowflake.snowpark.types as T
from joblib import dump, load
from sklearn.linear_model import LinearRegression
from snowflake.snowpark import Session, DataFrame

OUTPUTS = []


def train_model(session: Session, model_filepath: str) -> str:
    pce_df = session.table("BEANIPA")
    filtered_df = filter_personal_consumption_expenditures(pce_df)
    pce_pred = train_linear_regression_model(filtered_df.to_pandas())  # type: ignore
    dump_model(session, pce_pred)
    register_udf(session, model_filepath, "predict_pce_udf")
    return str(OUTPUTS)


# get PCE data
def filter_personal_consumption_expenditures(input_df: DataFrame) -> DataFrame:
    df_pce = (
        input_df.filter(
            F.col("Table_Name")
            == "Price Indexes For Personal Consumption Expenditures By Major Type Of Product"
        )
        .filter(F.col("Indicator_Name") == "Personal consumption expenditures (PCE)")
        .filter(F.col("Frequency") == "A")
        .filter(F.col("Date") >= "1972-01-01")
    )
    df_pce_year = df_pce.select(
        F.year(F.col("Date")).alias("Year"), F.col("Value").alias("PCE")
    )
    return df_pce_year


def train_linear_regression_model(input_pd: pd.DataFrame) -> LinearRegression:
    x = input_pd["YEAR"].to_numpy().reshape(-1, 1)
    y = input_pd["PCE"].to_numpy()

    model = LinearRegression().fit(x, y)

    # test model for 2023
    predictYear = 2023
    pce_pred = model.predict([[predictYear]])
    OUTPUTS.append(input_pd.tail())
    OUTPUTS.append(
        "Prediction for " + str(predictYear) + ": " + str(round(pce_pred[0], 2))
    )
    return model


def dump_model(session: Session, model: LinearRegression) -> None:
    model_output_dir = "/tmp"

    model_filepath = os.path.join(model_output_dir, "model.joblib")

    dump(model, model_filepath)

    session.file.put(model_filepath, "@deploy", overwrite=True, auto_compress=False)


@cachetools.cached(cache={})
def load_model(filename: str) -> LinearRegression:
    model_file_path = sys._xoptions.get("snowflake_import_directory") + filename

    return load(model_file_path)


def register_udf(session: Session, model_filepath: str, udf_name: str):
    def predict_pce(ps: pd.Series) -> pd.Series:
        model = load_model(model_filepath)

        return ps.transform(lambda x: model.predict([[x]])[0].round(2).astype(float))

    try:
        session.add_import(f"@deploy/{model_filepath}")
        session.udf.register(
            predict_pce,
            return_type=T.PandasSeriesType(T.FloatType()),
            input_types=[T.PandasSeriesType(T.IntegerType())],
            packages=[
                "pandas",
                "scikit-learn",
                "cachetools",
                "snowflake-snowpark-python",
            ],
            is_permanent=True,
            name=udf_name,
            replace=True,
            stage_location="@deploy",
        )
        OUTPUTS.append("UDF registered")

        return f"Model {model_filepath} registered successfully to UDF {udf_name}!"
    except Exception as e:
        return f"Model registration failed! Original error: {e}"


if __name__ == "__main__":
    from snowpark_devops.utils import get_session

    session = get_session.session()
    train_model(session, "model.joblib")
